/* console.log("Hello world");

    
for(let number = 0; number < 10; number++){
    if(number == 2){
        console.log("Number 2 is found skip next line of codes then continue to next iteration");
        continue;
    }
    if(number !=2){
        console.log(number);
    }
    if(number == 5){
        break;
    }
} */

/* 
1 initialize a value
2 checks the condition
3 runs the statement inside the loop
4 change the value (increment or decrement)
- go back to step 2

*/
/* console.log("--------------------");
// Reassigning with Concatenation of string
let myString = "dooooom";
let letterO  = "";

for(let i=0; i<myString.length; i++){
    if(myString[i] == 'o');{
        //        emptystring  +   o
        //        o       +     o
        //        oo      +     o
        //        ooo     +     o
        //        oooo    +     o
        //        ooooo   +     o
        //        oooooo  +           
       letterO = letterO + myString[i];
    }
}
console.log(letterO);
 */

//  Array
//  An array in programming is simply a list ofdata. Let's write the example earlier

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

// 
let studentNumbers = [ '2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

//  Common examples of arrays
// index       0      1      2    3
let grades = [98.5, 94.32, 89.2, 90.1, 99];
// index                0        1        2       3        4
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba',  'Fujitsu'];

// Possible use of an array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

//  Alternative ways to write arrays
let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake bootstrap'
];

console.log(myTasks);

// Creating an array wiith values from variables
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";


let cities = [
    'Tokyo',
    'Manila',
    'Jakarta'
];

console.log(cities);

//  [SECTION] length property
// The.length 

console.log(myTasks.length);
console.log(cities.length);


let balnkArr = [];
console.log(balnkArr.length);

//  legth property can also be used with strings. Some array methods and properties can also be used with strings

let fullName = "Jamie Oliver"
console.log(fullName.length);


//  Removing the last element in an array
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);


cities.length --;
console.log(cities.length);
console.log(cities);


//  However we can't do the same in string
console.log("Iniitial length of fullName: " + fullName.length);
fullName.length = fullName.length-1;
console.log("Current length of fullName: " + fullName.length);
console.log(fullName);

/* let theBeatles = ['John', 'Paul', 'george'];
theBeatles++;
console.log(theBeatles); */


//  accessing the element of an array through index
// let grades = [98.5, 94.32, 89.2, 90.1];
console.log(grades[0]); 

// let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba',  'Fujitsu'];

console.log(computerBrands[3]);



// let grades = [98.5, 94.32, 89.2, 90.1, 99];
function getGrade(index){
    console.log(grades[index]);
}

getGrade(3);
getGrade(grades.length-1);

let lakersLegend = ['kobe', 'Shaq','Lebron', 'Magic', 'Kareem'];
let currentlaker = lakersLegend[2];
console.log(currentlaker);


// Change an element
console.log('Array before reassigment');
console.log(lakersLegend);
lakersLegend[2] = 'Pau Gasol';
console.log("Array after reassignment");
console.log(lakersLegend);

//  Change the last element 
let bullslegend = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];
let lastElementIndex = bullslegend.length-1;
console.log(lastElementIndex);
 console.log(bullslegend[lastElementIndex]);

//  you could also access it directly
console.log(bullslegend[bullslegend.length-1]);
bullslegend[lastElementIndex] = "Harper";
console.log(bullslegend);


//  Add items into the array
// Adding elements after the last element

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[2] = "Tifa Lockhart";
console.log(newArr);
                    //  0           1           2           3 
                    //  1           2           3               
// Current output ['Cloud Strife', empty, 'Tifa Lockhart']
// Adding Element after last element

newArr[newArr.length] = "Barret Wallace";
console.log(newArr);



//  Looping over an array(dispaly the elements of an array)
for(let index=0; index<newArr.length; index++){
    console.log(newArr[index]); // 0 // 1 
}

let numArr = [5,12,30,46,40];

//  a loop that will check per elements is divisible by 5
for(let index =0; index<numArr.length; index++){
    if(numArr[index] %5 === 0 ){
        console.log(numArr[index] + " is divisible by 5");
    }
    else{
        console.log(numArr[index] + " is not divisible by 5");
    }
}


// [SECTION] Multidimensional Arrays

//  arrays inside an array
let chessBoard =[
    ['a1','b1','c1','d1','e1','f1','g1','h1'],
    ['a2','b2','c2','d2','e2','f2','g2','h2'],
    ['a3','b3','c3','d3','e3','f3','g3','h3'],
    ['a4','b4','c4','d4','e4','f4','g4','h4'],
    ['a5','b5','c5','d5','e5','f5','g5','h5'],
    ['a6','b6','c6','d6','e6','f6','g6','h6'],
    ['a7','b7','c7','d7','e7','f7','g7','h7'],
    ['a8','b8','c8','d8','e8','f8','g8','h8']
];


console.log(chessBoard);

console.log(chessBoard[1][4]);
//                     row col 

//  Mini activuty - Display c5
console.log(chessBoard[4][2]);
/* 
function getGrade(index){
    console.log(grades[index]);
}

getGrade(3);
getGrade(grades.length-1);
 */
































